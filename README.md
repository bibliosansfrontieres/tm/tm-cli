# tm-cli

Query the production stack from Command Line!

A simple wrapper over common `curl $api | jq .` needs.

## Installation

```shell
git clone https://gitlab.com/bibliosansfrontieres/tm/tm-cli /usr/local/src/
ln -s /usr/local/src/tm-cli/tmctl /usr/local/bin/tmctl
```

## Usage

```text
Usage: tmctl <action> <process_id>
Available actions:
  logs              DEBUG logs
  info              infos about the process_id
  errors            errors from a process_id
  status            status of a project_id
  omeka             Omeka project bound to this process_id
  mongodb           Connect to MongoDB (using the mongo-express Docker image)
  mongodbgate       Connect to MongoDB (using the dbgate/dbgate Docker image)
  packagesbuild     List packages that have been rebuild from a process_id
  packagescount     Count packages to be installed
  packageslist      List packages to be installed
  getkeys           Push the tinc/ssh keys from a device to bubble
  deploytargets     List targets for a Deploy process_id
  missingfilenames  Get missing files filenames from process_id
```
