#!/bin/bash

DEBUGMODE=${DEBUGMODE:-0}

readonly DESCRIPTOR_URL='http://olip.ideascube.org/amd64/descriptor.json'
readonly JSONFILE='/tmp/descriptor.json'


edebug() {
    [[ $DEBUGMODE == 0 ]] && return
    >&2 echo "[+] $*"
}

_show_usage() {
    echo "Usage: $( basename "$0") <action> [arg]
Generic actions:
  update            Update the descriptor.json file

Mediacenter actions: arg = content_id
  hugolist          List all Hugo packages
  hugo              Get a Hugo package
  hugoversion       Version from a Hugo package
  hugodate          Version from a Hugo package - human format
  hugosize          Size of a Hugo package
"
}


#
# HELPERS
#

_jq() {
    jq "$*" "${JSONFILE}"
}

# arg: An application .bundle - i.e. "mediacenter.app"
_get_application() {
    local app=$1
    _jq '.applications[] | select(.bundle=="'"${app}"'")'
}


#
# GENERIC ACTIONS
#

_update() {
    wget --quiet --no-config "${DESCRIPTOR_URL}" -O "${JSONFILE}" || {
        >&2 echo "Error: could not download the descriptor file."
        exit 2
    }
}


#
# MEDIACENTER ACTIONS
#

_hugo() {
    local paquet=$1
    _get_application 'mediacenter.app' | jq '.contents[] | select(.content_id=="'"${paquet}"'")'
}
_hugolist() {
    _get_application 'mediacenter.app' | jq -r '.contents[].content_id'
}
_hugoversion() {
    local paquet=$1
    _hugo "$paquet" | jq -r '.version'
}
_hugodate() {
    local paquet=$1
    date "+%F %T" -d "@$( _hugoversion "$paquet" )"
}
_hugosize() {
    local paquet=$1
    _hugo "$paquet" | jq -r '.size'
}


#
# MAIN
#

action=$1
edebug "action is $action"
[ -z "$action" ] && {
    _show_usage
    exit 0
}
arg=$2
edebug "arg is $arg"

case $action in
    hugo)
        edebug "go hugo"
        _hugo "$arg"
        ;;
    hugolist)
        edebug "go hugolist"
        _hugolist "$arg"
        ;;
    hugoversion)
        edebug "go hugoversion"
        _hugoversion "$arg"
        ;;
    hugodate)
        edebug "go hugodate"
        _hugodate "$arg"
        ;;
    hugosize)
        edebug "go hugosize"
        _hugosize "$arg"
        ;;
    update)
        edebug "go update"
        _update
        ;;

    *)
        edebug "go help"
        >&2 echo "Error: unknown action: $action"
        _show_usage
        exit 1
        ;;
esac
